FROM alpine:latest

RUN apk update && apk add openssh-client git

COPY known_hosts /root/.ssh/known_hosts

RUN chmod 644 /root/.ssh/known_hosts

CMD ["sh"]